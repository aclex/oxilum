/*
 * Oxilum
 * Copyright (C) 2019 Alexey Chernov <4ernov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include <bcm_host.h>

#include <oxilum/omx/component/base.h>
#include <oxilum/omx/port/base.h>

using namespace std;

using namespace oxilum::omx;

namespace
{

}

int main(int, char**)
{
	bcm_host_init();

	component::base c("OMX.broadcom.video_encode");

	const version_info v { c.version() };

	cout << "Component version information:" << endl;
	cout << v << endl;

	cout << "Ports:" << endl;
	for (const auto& port : c.ports())
	{
		cout << port.index() << endl;
	}

	cout << "Enabling ports…" << endl;

	for (auto& port : c.ports())
	{
		port.enable();
	}

	c.set_state(state::idle);

	c.set_state(state::executing);
}
