/*
 * Oxilum
 * Copyright (C) 2019 Alexey Chernov <4ernov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "oxilum/omx/error.h"

using namespace std;

string oxilum::omx::to_string(const OMX_ERRORTYPE error)
{
	switch (error)
	{
	case OMX_ErrorNone:
		return "OMX_ErrorNone"s;

	case OMX_ErrorInsufficientResources:
		return "OMX_ErrorInsufficientResources"s;

	case OMX_ErrorUndefined:
		return "OMX_ErrorUndefined"s;

	case OMX_ErrorInvalidComponentName:
		return "OMX_ErrorInvalidComponentName"s;

	case OMX_ErrorComponentNotFound:
		return "OMX_ErrorComponentNotFound"s;

	case OMX_ErrorInvalidComponent:
		return "OMX_ErrorInvalidComponent"s;

	case OMX_ErrorBadParameter:
		return "OMX_ErrorBadParameter"s;

	case OMX_ErrorNotImplemented:
		return "OMX_ErrorNotImplemented"s;

	case OMX_ErrorUnderflow:
		return "OMX_ErrorUnderflow"s;

	case OMX_ErrorOverflow:
		return "OMX_ErrorOverflow"s;

	case OMX_ErrorHardware:
		return "OMX_ErrorHardware"s;

	case OMX_ErrorInvalidState:
		return "OMX_ErrorInvalidState"s;

	case OMX_ErrorStreamCorrupt:
		return "OMX_ErrorStreamCorrupt"s;

	case OMX_ErrorPortsNotCompatible:
		return "OMX_ErrorPortsNotCompatible"s;

	case OMX_ErrorResourcesLost:
		return "OMX_ErrorResourcesLost"s;

	case OMX_ErrorNoMore:
		return "OMX_ErrorNoMore"s;

	case OMX_ErrorVersionMismatch:
		return "OMX_ErrorVersionMismatch"s;

	case OMX_ErrorNotReady:
		return "OMX_ErrorNotReady"s;

	case OMX_ErrorTimeout:
		return "OMX_ErrorTimeout"s;

	case OMX_ErrorSameState:
		return "OMX_ErrorSameState"s;

	case OMX_ErrorResourcesPreempted:
		return "OMX_ErrorResourcesPreempted"s;

	case OMX_ErrorPortUnresponsiveDuringAllocation:
		return "OMX_ErrorPortUnresponsiveDuringAllocation"s;

	case OMX_ErrorPortUnresponsiveDuringDeallocation:
		return "OMX_ErrorPortUnresponsiveDuringDeallocation"s;

	case OMX_ErrorPortUnresponsiveDuringStop:
		return "OMX_ErrorPortUnresponsiveDuringStop"s;

	case OMX_ErrorIncorrectStateTransition:
		return "OMX_ErrorIncorrectStateTransition"s;

	case OMX_ErrorIncorrectStateOperation:
		return "OMX_ErrorIncorrectStateOperation"s;

	case OMX_ErrorUnsupportedSetting:
		return "OMX_ErrorUnsupportedSetting"s;

	case OMX_ErrorUnsupportedIndex:
		return "OMX_ErrorUnsupportedIndex"s;

	case OMX_ErrorBadPortIndex:
		return "OMX_ErrorBadPortIndex"s;

	case OMX_ErrorPortUnpopulated:
		return "OMX_ErrorPortUnpopulated"s;

	case OMX_ErrorComponentSuspended:
		return "OMX_ErrorComponentSuspended"s;

	case OMX_ErrorDynamicResourcesUnavailable:
		return "OMX_ErrorDynamicResourcesUnavailable"s;

	case OMX_ErrorMbErrorsInFrame:
		return "OMX_ErrorMbErrorsInFrame"s;

	case OMX_ErrorFormatNotDetected:
		return "OMX_ErrorFormatNotDetected"s;

	case OMX_ErrorContentPipeOpenFailed:
		return "OMX_ErrorContentPipeOpenFailed"s;

	case OMX_ErrorContentPipeCreationFailed:
		return "OMX_ErrorContentPipeCreationFailed"s;

	case OMX_ErrorSeperateTablesUsed:
		return "OMX_ErrorSeperateTablesUsed"s;

	case OMX_ErrorTunnelingUnsupported:
		return "OMX_ErrorTunnelingUnsupported"s;

	case OMX_ErrorKhronosExtensions:
		return "OMX_ErrorKhronosExtensions"s;

	case OMX_ErrorVendorStartUnused:
		return "OMX_ErrorVendorStartUnused"s;

	default:
		return "unknown<"s + std::to_string(error) + '>';
	}
}
