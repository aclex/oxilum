/*
 * Oxilum
 * Copyright (C) 2019 Alexey Chernov <4ernov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "oxilum/omx/port/base.h"

#include <algorithm>
#include <cassert>

#include "oxilum/omx/buffer.h"
#include "oxilum/omx/error.h"

#include "oxilum/omx/component/base.h"

using namespace std;

using namespace oxilum::omx;
using namespace oxilum::omx::port;

base::base(component::base& comp, const size_t index) noexcept :
	m_comp(comp),
	m_index(index)
{

}

base::base(base&& a) noexcept :
	m_busy_omx_buffers(move(a.m_busy_omx_buffers)),
	m_busy_client_buffers(move(a.m_busy_client_buffers)),
	m_free_buffers(move(a.m_free_buffers)),
	m_comp(a.m_comp),
	m_index(a.m_index)
{

}

future<void> base::enable()
{
	const OMX_ERRORTYPE status { OMX_SendCommand(m_comp.handle(), OMX_CommandPortEnable, m_index, nullptr) };

	if (status != OMX_ErrorNone)
	{
		const string msg { "Error enabling OpenMAX component port, return code: "s + to_string(status) + '.' };
		throw runtime_error(msg);
	}

	promise<void> result_promise;
	future<void>&& ret { result_promise.get_future() };

	component::base::event_details ed
	{
		OMX_EventCmdComplete,
		OMX_CommandPortEnable,
		static_cast<OMX_U32>(m_index)
	};

	m_comp.keep_event_promise(std::move(ed), std::move(result_promise));

	return std::move(ret);
}

future<void> base::disable()
{
	const OMX_ERRORTYPE status { OMX_SendCommand(m_comp.handle(), OMX_CommandPortDisable, m_index, nullptr) };

	if (status != OMX_ErrorNone)
	{
		const string msg { "Error disabling OpenMAX component port, return code: "s + to_string(status) + '.' };
		throw runtime_error(msg);
	}

	promise<void> result_promise;
	future<void>&& ret { result_promise.get_future() };

	component::base::event_details ed
	{
		OMX_EventCmdComplete,
		OMX_CommandPortDisable,
		static_cast<OMX_U32>(m_index)
	};

	m_comp.keep_event_promise(std::move(ed), std::move(result_promise));

	return std::move(ret);
}


void base::allocate_buffers()
{
	if (!m_free_buffers.empty() || !m_busy_client_buffers.empty() || !m_busy_omx_buffers.empty())
	{
		// already allocated, don't repeat for idempotency
		return;
	}

	OMX_PARAM_PORTDEFINITIONTYPE portdef { };

	portdef.nSize = sizeof(OMX_PARAM_PORTDEFINITIONTYPE);
	portdef.nVersion.nVersion = OMX_VERSION;
	portdef.nPortIndex = m_index;

	OMX_ERRORTYPE status { OMX_ErrorNone };

	status = OMX_GetParameter(m_comp.handle(), OMX_IndexParamPortDefinition, &portdef);

	if (status != OMX_ErrorNone)
	{
		const string msg { "Can't get port " + std::to_string(m_index) + " parameters while allocating its buffers." };
		throw runtime_error(msg);
	}

	for (size_t i = 0; i < portdef.nBufferCountActual; ++i)
		m_free_buffers.emplace_back(m_comp, *this, portdef.nBufferSize);
}

void base::deallocate_buffers()
{
	m_free_buffers.clear();
	assert(m_busy_client_buffers.size() == 0);
	assert(m_busy_omx_buffers.size() == 0);
}

buffer& base::get_buffer()
{
	unique_lock<mutex> lock(m_mutex);

	m_cond.wait(lock, [this]() { return !m_free_buffers.empty(); });

	m_busy_client_buffers.push_back(move(m_free_buffers.front()));
	m_free_buffers.pop_front();

	return m_busy_client_buffers.back();
}

void base::hand_to_omx(buffer& buf)
{
	unique_lock<mutex> lock(m_mutex);

	const auto it { find(begin(m_busy_client_buffers), end(m_busy_client_buffers), buf) };

	assert(it != end(m_busy_client_buffers));

	m_busy_omx_buffers.push_back(move(*it));
	m_busy_client_buffers.erase(it);
}

void base::return_from_omx(OMX_BUFFERHEADERTYPE* pBuffer)
{
	unique_lock<mutex> lock(m_mutex);

	const auto it { find(begin(m_busy_omx_buffers), end(m_busy_omx_buffers), pBuffer) };

	assert(it != end(m_busy_omx_buffers));

	m_free_buffers.push_back(move(*it));
	m_busy_omx_buffers.erase(it);

	m_cond.notify_one();
}
