/*
 * Oxilum
 * Copyright (C) 2019 Alexey Chernov <4ernov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "oxilum/omx/buffer.h"

#include <iostream>

#include "oxilum/omx/component/base.h"
#include "oxilum/omx/port/base.h"

#include "oxilum/omx/error.h"

using namespace std;

using namespace oxilum::omx;

buffer::buffer(component::base& comp, port::base& port, const size_t size) :
	m_comp(comp),
	m_port(port),
	m_busy()
{
	const OMX_ERRORTYPE status { OMX_AllocateBuffer(comp.handle(), &m_header, static_cast<OMX_U32>(port.index()), nullptr, static_cast<OMX_U32>(size)) };

	if (status != OMX_ErrorNone)
	{
		const string msg { "Error during buffer allocation, return code is " + to_string(status) + '.' };
		throw runtime_error(msg);
	}
}

void buffer::empty()
{
	OMX_EmptyThisBuffer(m_comp.handle(), m_header);
}

void buffer::fill()
{
	OMX_FillThisBuffer(m_comp.handle(), m_header);
}

bool buffer::operator==(const OMX_BUFFERHEADERTYPE* const b) const noexcept
{
	return m_header == b;
}

bool buffer::operator!=(const OMX_BUFFERHEADERTYPE* const b) const noexcept
{
	return !operator==(b);
}

bool buffer::operator==(const buffer& b) const noexcept
{
	return m_header == b.m_header;
}

bool buffer::operator!=(const buffer& b) const noexcept
{
	return !operator==(b);
}

buffer::~buffer()
{
	const OMX_ERRORTYPE status { OMX_FreeBuffer(m_comp.handle(), static_cast<OMX_U32>(m_port.index()), m_header) };

	if (status != OMX_ErrorNone)
	{
		const string msg { "Error during buffer allocation, return code is " + to_string(status) + '.' };
		cerr << msg << endl;
	}
}
