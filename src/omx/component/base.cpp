/*
 * Oxilum
 * Copyright (C) 2019 Alexey Chernov <4ernov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "oxilum/omx/component/base.h"

#include <algorithm>
#include <cassert>
#include <iostream>

#include "oxilum/omx/port/base.h"

#include "oxilum/omx/buffer.h"
#include "oxilum/omx/error.h"

using namespace std;

using namespace oxilum;
using namespace oxilum::omx::component;

namespace
{
	OMX_INDEXTYPE g_index_types[]
	{
		OMX_IndexParamAudioInit,
		OMX_IndexParamVideoInit,
		OMX_IndexParamImageInit,
		OMX_IndexParamOtherInit
	};
}

bool base::event_details::operator==(const base::event_details& b) const noexcept
{
	return (type == b.type &&
		data1 == b.data1 &&
		data2 == b.data2);
}

bool base::event_details::operator!=(const base::event_details& b) const noexcept
{
	return !operator==(b);
}

base::base(const string& name) :
	m_name(name),
	m_current_state(omx::state::unloaded)
{
	OMX_CALLBACKTYPE callbacks
	{
		&base::event_handler, // EventHandler
		&base::empty_buffer_done_handler, // EmptyBufferDone
		&base::fill_buffer_done_handler // FillBufferDone
	};

	const OMX_ERRORTYPE status { OMX_GetHandle(&m_comp, const_cast<OMX_STRING>(m_name.c_str()), this, &callbacks) };

	if (status != OMX_ErrorNone)
	{
		const string msg { "Error initializing OpenMAX component, return code: "s + to_string(status) + '.' };
		throw runtime_error(msg);
	}

	m_current_state = omx::state::loaded;
	m_version_info = get_version_info();
	m_ports = enumerate_ports();
}

oxilum::omx::version_info base::version() const
{
	return m_version_info;
}

future<void> base::set_state(const state st)
{
	promise<void> result_promise;
	future<void>&& ret { result_promise.get_future() };

	event_details ed { OMX_EventCmdComplete, OMX_CommandStateSet, to_omx_state_type(st) };

	keep_event_promise(move(ed), move(result_promise));

	const OMX_ERRORTYPE status { OMX_SendCommand(m_comp, OMX_CommandStateSet, to_omx_state_type(st), nullptr) };

	if (status != OMX_ErrorNone)
	{
		const string msg { "Error setting component state, return code: "s + to_string(status) + '.' };
		throw runtime_error(msg);
	}

	process_state_transition(m_current_state, st);

	return move(ret);
}

oxilum::omx::version_info base::get_version_info()
{
	char name[128];
	OMX_VERSIONTYPE comp, spec;
	OMX_UUIDTYPE uuid;

	const OMX_ERRORTYPE status { OMX_GetComponentVersion(m_comp, name, &comp, &spec, &uuid) };

	if (status != OMX_ErrorNone)
	{
		const string msg { "Error getting OpenMAX component version, return code: "s + to_string(status) + '.' };
		throw runtime_error(msg);
	}

	return version_info { name, { comp.nVersion }, { spec.nVersion }, reinterpret_cast<char*>(uuid) };
}

vector<omx::port::base> base::enumerate_ports()
{
	vector<port::base> port_suite;

	OMX_PORT_PARAM_TYPE ports;

	ports.nSize = sizeof(OMX_PORT_PARAM_TYPE);
	ports.nVersion.nVersion = OMX_VERSION;

	for (const auto index_type : g_index_types)
	{
		const OMX_ERRORTYPE status { OMX_GetParameter(m_comp, index_type, &ports) };
		if (status == OMX_ErrorNone)
		{
			for (size_t index = ports.nStartPortNumber; index < ports.nStartPortNumber + ports.nPorts; ++index)
			{
				port_suite.emplace_back(*this, index);
			}
		}
	}

	return move(port_suite);
}

void base::keep_event_promise(event_details&& ed, promise<void>&& promise)
{
	lock_guard<mutex> lock(m_pending_promises_mutex);
	m_pending_promises.emplace(move(ed), move(promise));
}

void base::process_state_transition(const omx::state from, const omx::state to)
{
	if (from == omx::state::loaded && to == omx::state::idle)
	{
		for (auto& port : m_ports)
		{
			port.allocate_buffers();
		}
	}

	if (from == omx::state::idle && to == omx::state::loaded)
	{
		for (auto& port : m_ports)
		{
			port.deallocate_buffers();
		}
	}
}

void base::update_state(OMX_EVENTTYPE eEvent, OMX_U32 data1, OMX_U32 data2, OMX_PTR pEventData)
{
	if (eEvent == OMX_EventCmdComplete && data1 == OMX_CommandStateSet)
	{
		m_current_state = from_omx_state_type(static_cast<OMX_STATETYPE>(data2));
	}
}

void base::look_after_promises(OMX_EVENTTYPE eEvent, OMX_U32 data1, OMX_U32 data2, OMX_PTR pEventData)
{
	const event_details my_details { eEvent, data1, data2 };
	lock_guard<mutex> lock(m_pending_promises_mutex);

	const auto& it { m_pending_promises.find(my_details) };

	if (it != end(m_pending_promises))
	{
		it->second.set_value();
		m_pending_promises.erase(it);
	}
}

void base::handle_event(OMX_EVENTTYPE eEvent, OMX_U32 data1, OMX_U32 data2, OMX_PTR pEventData)
{
	update_state(eEvent, data1, data2, pEventData);
	look_after_promises(eEvent, data1, data2, pEventData);
}

void base::handle_buffer_processed(OMX_BUFFERHEADERTYPE* pBuffer)
{
	const size_t port_no { pBuffer->nInputPortIndex };

	const auto it { find_if(begin(m_ports), end(m_ports),
			[port_no](const port::base& p) { return p.index() == port_no; }) };

	if (it == end(m_ports))
	{
		const string msg { "Error returning buffer from OpenMAX." };
		throw runtime_error(msg);
	}

	it->return_from_omx(pBuffer);
}

void base::handle_buffer_empty(OMX_BUFFERHEADERTYPE* pBuffer)
{
	handle_buffer_processed(pBuffer);
}

void base::handle_buffer_filled(OMX_BUFFERHEADERTYPE* pBuffer)
{
	handle_buffer_processed(pBuffer);
}

OMX_ERRORTYPE base::event_handler(OMX_HANDLETYPE hComponent, OMX_PTR pAppData, OMX_EVENTTYPE eEvent, OMX_U32 data1, OMX_U32 data2, OMX_PTR pEventData)
{
	base* const obj { static_cast<base*>(pAppData) };

	assert(obj->m_comp == hComponent);

	obj->handle_event(eEvent, data1, data2, pEventData);

	return OMX_ErrorNone;
}

OMX_ERRORTYPE base::empty_buffer_done_handler(OMX_HANDLETYPE hComponent, OMX_PTR pAppData, OMX_BUFFERHEADERTYPE* pBuffer)
{
	base* const obj { static_cast<base*>(pAppData) };

	assert(obj->m_comp == hComponent);

	obj->handle_buffer_empty(pBuffer);

	return OMX_ErrorNone;
}

OMX_ERRORTYPE base::fill_buffer_done_handler(OMX_HANDLETYPE hComponent, OMX_PTR pAppData, OMX_BUFFERHEADERTYPE* pBuffer)
{
	base* const obj { static_cast<base*>(pAppData) };

	assert(obj->m_comp == hComponent);

	obj->handle_buffer_filled(pBuffer);

	return OMX_ErrorNone;
}

void base::default_to_loaded_state() noexcept
{
	if (m_current_state == omx::state::loaded)
	{
		return;
	}

	if (m_current_state == omx::state::executing || m_current_state == omx::state::paused)
	{
		set_state(omx::state::idle).wait();
	}

	set_state(omx::state::loaded).wait();
}

base::~base()
{
	default_to_loaded_state();

	const OMX_ERRORTYPE status { OMX_FreeHandle(m_comp) };

	if (status != OMX_ErrorNone)
	{
		const string msg { "Error deinitializing OpenMAX component, return code: "s + to_string(status) + '.' };
		cerr << msg << endl;
	}
}
