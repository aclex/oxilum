/*
 * Oxilum
 * Copyright (C) 2019 Alexey Chernov <4ernov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef OXILUM_OMX_VERSION_H
#define OXILUM_OMX_VERSION_H

#include <string>
#include <iostream>
#include <cstdint>

#include <OMX_Types.h>

namespace oxilum::omx
{
	struct version
	{
		std::uint32_t word;

		constexpr unsigned char major() const noexcept { return word >> 24 & 0xff; }
		constexpr unsigned char minor() const noexcept { return word >> 16 & 0xff; }
		constexpr unsigned char revision() const noexcept { return word >> 8 & 0xff; }
		constexpr unsigned char step() const noexcept { return word & 0xff; }

		template<typename Ch> friend std::basic_ostream<Ch>& operator<<(std::basic_ostream<Ch>& os, const version& v)
		{
			os << static_cast<int>(v.major()) << '.' << static_cast<int>(v.minor()) << '.' << static_cast<int>(v.revision()) << '.' << static_cast<int>(v.step());

			return os;
		}
	};

	struct version_info
	{
		std::string reference_name;
		version component_version, spec_version;
		std::string uuid;

		template<typename Ch> friend std::basic_ostream<Ch>& operator<<(std::basic_ostream<Ch>& os, const version_info& v)
		{
			os << "OpenMAX reference name: " << v.reference_name << std::endl;
			os << "component version: " << v.component_version << std::endl;
			os << "OpenMAX specification version: " << v.spec_version << std::endl;
			os << "component instance UUID: " << v.uuid << std::endl;

			return os;
		}
	};

	constexpr version header_version { OMX_VERSION };
}

#endif // OXILUM_OMX_VERSION_H
