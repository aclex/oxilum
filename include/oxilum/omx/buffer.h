/*
 * Oxilum
 * Copyright (C) 2019 Alexey Chernov <4ernov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef OXILUM_OMX_BUFFER_H
#define OXILUM_OMX_BUFFER_H

#include <cstdint>
#include <cstddef>

#include <OMX_Core.h>

namespace oxilum::omx
{
	namespace component
	{
		class base;
	}

	namespace port
	{
		class base;
	}

	class buffer
	{
	public:
		explicit buffer(omx::component::base& comp, omx::port::base& port, const std::size_t size);
		std::uint8_t* data() noexcept { return m_header->pBuffer; }
		std::size_t size() noexcept { return m_header->nSize; }
		void empty();
		void fill();
		bool operator==(const OMX_BUFFERHEADERTYPE* const b) const noexcept;
		bool operator!=(const OMX_BUFFERHEADERTYPE* const b) const noexcept;
		bool operator==(const buffer& b) const noexcept;
		bool operator!=(const buffer& b) const noexcept;
		~buffer();

	private:
		OMX_BUFFERHEADERTYPE* m_header;
		omx::component::base& m_comp;
		omx::port::base& m_port;
		bool m_busy;
	};
}

#endif // OXILUM_OMX_BUFFER_H
