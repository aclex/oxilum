/*
 * Oxilum
 * Copyright (C) 2019 Alexey Chernov <4ernov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef OXILUM_OMX_STATE_H
#define OXILUM_OMX_STATE_H

#include <OMX_Core.h>

namespace oxilum::omx
{
	enum class state
	{
		invalid,
		loaded,
		idle,
		executing,
		paused,
		waiting_for_resources,
		unloaded
	};

	constexpr OMX_STATETYPE to_omx_state_type(const state st) noexcept
	{
		switch (st)
		{
		default:
		case state::invalid:
			return OMX_StateInvalid;

		case state::loaded:
			return OMX_StateLoaded;

		case state::idle:
			return OMX_StateIdle;

		case state::executing:
			return OMX_StateExecuting;

		case state::paused:
			return OMX_StatePause;

		case state::waiting_for_resources:
			return OMX_StateWaitForResources;
		}
	}

	constexpr state from_omx_state_type(const OMX_STATETYPE st) noexcept
	{
		switch (st)
		{
		default:
		case OMX_StateInvalid:
			return state::invalid;

		case OMX_StateLoaded:
			return state::loaded;

		case OMX_StateIdle:
			return state::idle;

		case OMX_StateExecuting:
			return state::executing;

		case OMX_StatePause:
			return state::paused;

		case OMX_StateWaitForResources:
			return state::waiting_for_resources;
		}
	}
}

#endif // OXILUM_OMX_STATE_H
