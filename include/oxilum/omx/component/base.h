/*
 * Oxilum
 * Copyright (C) 2019 Alexey Chernov <4ernov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef OXILUM_OMX_COMPONENT_BASE_H
#define OXILUM_OMX_COMPONENT_BASE_H

#include <string>
#include <vector>
#include <unordered_map>
#include <functional>
#include <future>
#include <cstddef>

#include <OMX_Component.h>

#include <oxilum/omx/state.h>
#include <oxilum/omx/version.h>
#include <oxilum/omx/init.h>

namespace oxilum::omx::port
{
	class base;
}

namespace oxilum::omx::component
{
	class base;

	class base
	{
	public:
		explicit base(const std::string& name);
		version_info version() const;

		std::vector<port::base>& ports() noexcept { return m_ports; }
		std::future<void> set_state(const state st);

		OMX_HANDLETYPE handle() noexcept { return m_comp; }
		const OMX_HANDLETYPE handle() const noexcept { return m_comp; }

		~base();

	private:
		friend class port::base;

		struct event_details
		{
			OMX_EVENTTYPE type;
			OMX_U32 data1;
			OMX_U32 data2;

			bool operator==(const event_details& b) const noexcept;
			bool operator!=(const event_details& b) const noexcept;

			struct hash
			{
				std::size_t operator()(const event_details& k) const noexcept
				{
					using std::hash;

					return ((hash<int>()(k.type) ^
							(hash<std::uint32_t>()(k.data1) >> 1) ^
							(hash<std::uint32_t>()(k.data2) << 1)));
				}
			};

		};

		base(const base&) = delete;
		base& operator=(const base&) = delete;

		version_info get_version_info();
		std::vector<port::base> enumerate_ports();
		void keep_event_promise(event_details&& ed, std::promise<void>&& promise);

		void process_state_transition(const omx::state from, const omx::state to);

		void update_state(OMX_EVENTTYPE eEvent, OMX_U32 data1, OMX_U32 data2, OMX_PTR pEventData);
		void look_after_promises(OMX_EVENTTYPE eEvent, OMX_U32 data1, OMX_U32 data2, OMX_PTR pEventData);

		void default_to_loaded_state() noexcept;

		void handle_event(OMX_EVENTTYPE eEvent, OMX_U32 data1, OMX_U32 data2, OMX_PTR pEventData);
		void handle_buffer_processed(OMX_BUFFERHEADERTYPE* pBuffer);
		void handle_buffer_empty(OMX_BUFFERHEADERTYPE* pBuffer);
		void handle_buffer_filled(OMX_BUFFERHEADERTYPE* pBuffer);

		static OMX_ERRORTYPE event_handler(OMX_HANDLETYPE hComponent, OMX_PTR pAppData, OMX_EVENTTYPE eEvent, OMX_U32 data1, OMX_U32 data2, OMX_PTR pEventData);
		static OMX_ERRORTYPE empty_buffer_done_handler(OMX_HANDLETYPE hComponent, OMX_PTR pAppData, OMX_BUFFERHEADERTYPE* pBuffer);
		static OMX_ERRORTYPE fill_buffer_done_handler(OMX_HANDLETYPE hComponent, OMX_PTR pAppData, OMX_BUFFERHEADERTYPE* pBuffer);

		const initer m_initer;

		const std::string m_name;

		std::vector<port::base> m_ports;

		std::unordered_map<event_details, std::promise<void>, event_details::hash> m_pending_promises;
		std::mutex m_pending_promises_mutex;

		version_info m_version_info;

		omx::state m_current_state;

		OMX_HANDLETYPE m_comp;
	};
}

#endif // OXILUM_OMX_COMPONENT_BASE_H
