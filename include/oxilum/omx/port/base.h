/*
 * Oxilum
 * Copyright (C) 2019 Alexey Chernov <4ernov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef OXILUM_OMX_PORT_BASE_H
#define OXILUM_OMX_PORT_BASE_H

#include <list>
#include <mutex>
#include <condition_variable>
#include <future>
#include <cstddef>

#include <OMX_Core.h>

namespace oxilum::omx
{
	namespace component
	{
		class base;
	}

	class buffer;
}

namespace oxilum::omx::port
{
	class base
	{
	public:
		explicit base(component::base& comp, const std::size_t index) noexcept;
		base(const base&) = delete;
		base& operator=(const base&) = delete;
		base(base&& b) noexcept;

		std::size_t index() const noexcept { return m_index; }
		std::future<void> enable();
		std::future<void> disable();
		void allocate_buffers();
		void deallocate_buffers();
		buffer& get_buffer();

	private:
		friend class component::base;

		void hand_to_omx(buffer& buf);
		void return_from_omx(OMX_BUFFERHEADERTYPE* pBuffer);

		std::list<buffer> m_busy_omx_buffers, m_busy_client_buffers, m_free_buffers;
		std::mutex m_mutex;
		std::condition_variable m_cond;

		component::base& m_comp;
		const std::size_t m_index;
	};
}

#endif // OXILUM_OMX_PORT_BASE_H
